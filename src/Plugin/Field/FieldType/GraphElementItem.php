<?php

namespace Drupal\graph_element\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'graph_element_item' field type.
 *
 * @FieldType(
 *   id = "graph_element_item",
 *   label = @Translation("Chart external resources"),
 *   module = "graph_element",
 *   description = @Translation("Chart external resources field type."),
 *   default_widget = "graph_element_widget",
 *   default_formatter = "graph_resources_formatter"
 * )
 */
class GraphElementItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'graph_title' => [
          'type' => 'varchar',
          'length' => 256,
        ],
        'graph_source' => [
          'type' => 'varchar',
          'length' => 256,
        ],
        'graph_type' => [
          'type' => 'varchar',
          'length' => 256,
        ],
        'graph_description' => [
          'type' => 'varchar',
          'length' => 256,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('graph_source')->getValue() && $this->get('graph_type')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['graph_title'] = DataDefinition::create('string')
      ->setLabel('Chart Title')
      ->setRequired(FALSE);
    $properties['graph_source'] = DataDefinition::create('string')
      ->setLabel('Chart Source')
      ->setRequired(TRUE);
    $properties['graph_type'] = DataDefinition::create('string')
      ->setLabel('Chart Type')
      ->setRequired(TRUE);
    $properties['graph_description'] = DataDefinition::create('string')
      ->setLabel('Chart Description')
      ->setRequired(FALSE);
    return $properties;
  }

}
