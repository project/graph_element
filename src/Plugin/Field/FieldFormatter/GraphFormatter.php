<?php

namespace Drupal\graph_element\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\graph_element\Utility;

/**
 * Plugin implementation of the 'graph_resources_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "graph_resources_formatter",
 *   label = @Translation("Chart Resources Formatter"),
 *   module = "graph_element",
 *   field_types = {
 *     "graph_element_item"
 *   }
 * )
 */
class GraphFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $utility = new Utility();
    $elements = [];
    $graphResources = $utility->getGraphResources();

    foreach ($items as $delta => $item) {
      $graphXKey = '';
      $graphYKey = '';
      $configBlock = $item->getValue();
      $configBlock['graph_source'] = $configBlock['graph_source'] ?? 'example';
      $configBlock['graph_type'] = $configBlock['graph_type'] ?? 'line';
      $configBlock['graph_title'] = $configBlock['graph_title'] ?? '';
      $configBlock['graph_description'] = $configBlock['graph_description'] ?? '';
      // Prefix markup.
      $title = $configBlock['graph_title'] ?? '';
      $prefix = '<div class="block_graph_prefix_' . $configBlock['graph_type'] . '">';
      $prefix .= '<h3 class="graph_title">' . $title . '</h3>';
      $prefix .= $configBlock['graph_description'] ?? '';
      $prefix .= '</div>';

      // Get the CSV contents.
      if ($configBlock['graph_source'] == "example") {
        $file_contents = $utility->getCsvContents();
        $gaphDataXKey = '8.x-3.x';
        $graphDataYKey = 'Week';
        $data = array_reverse($file_contents['8.x-3.x']);
        $labels = array_reverse($file_contents['Week']);
      }
      else {
        $gaphDataXKey = $graphResources[$configBlock['graph_source']]->x_key_name;
        $graphXKey = $graphResources[$configBlock['graph_source']]->x_key;
        $graphDataYKey = $graphResources[$configBlock['graph_source']]->y_key_name;
        $graphYKey = $graphResources[$configBlock['graph_source']]->y_key;
        $endpoint = $graphResources[$configBlock['graph_source']]->endpoint;
        $method = $graphResources[$configBlock['graph_source']]->method;
        $file_contents = $utility->statsApiCall($graphXKey, $graphYKey, $endpoint, $method);
        $data = $file_contents[$graphYKey];
        $labels = $file_contents[$graphXKey];
      }

      $idPrefix = "graph_field_";

      $chart = $utility->renderChart($prefix, $configBlock, $gaphDataXKey, $graphDataYKey, $data, $labels, $idPrefix);

      $elements[$delta] = [
        // '#theme' => 'field__graph_element_item',
        'elementChildren' => [
          '#prefix' => '<div class="graph_element_wrapper" id="graph_field_' . $delta . '_' . $graphXKey . '_' . $graphYKey . '">',
          '#suffix' => '</div>',
          'chart' => $chart,
        ],
      ];
    }
    return $elements;
  }

}
