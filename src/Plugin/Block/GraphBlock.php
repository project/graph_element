<?php

namespace Drupal\graph_element\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\graph_element\Utility;

/**
 * Provides a 'Chart' Block.
 *
 * @Block(
 *   id = "graph_block",
 *   admin_label = @Translation("Chart external resources block"),
 *   category = @Translation("Charts Blocks"),
 * )
 */
class GraphBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $configBlock = $this->getConfiguration();
    $utility = new Utility();

    if (!isset($configBlock['graph_source']) && empty($configBlock['graph_source'])) {
      return [];
    }
    $graphResources = $utility->getGraphResources();
    // Prefix markup.
    $title = $configBlock['graph_title'] ?? '';
    $prefix = '<div class="block_graph_prefix_' . $configBlock['graph_type'] . '">';
    $prefix .= '<h3 class="graph_title">' . $title . '</h3>';
    $prefix .= isset($configBlock['graph_description']) && $configBlock['graph_description']['value'] ? $configBlock['graph_description']['value'] : '';
    $prefix .= '</div>';

    // Get the CSV contents.
    if ($configBlock['graph_source'] == "example") {
      $file_contents = $utility->getCsvContents();
      $gaphDataKey = '8.x-3.x';
      $graphDataYKey = 'Week';
      $data = array_reverse($file_contents['8.x-3.x']);
      $labels = array_reverse($file_contents['Week']);
    }
    else {
      $gaphDataKey = $graphResources[$configBlock['graph_source']]->x_key_name;
      $graphXKey = $graphResources[$configBlock['graph_source']]->x_key;
      $graphDataYKey = $graphResources[$configBlock['graph_source']]->y_key_name;
      $graphYKey = $graphResources[$configBlock['graph_source']]->y_key;
      $endpoint = $graphResources[$configBlock['graph_source']]->endpoint;
      $method = $graphResources[$configBlock['graph_source']]->method;
      $file_contents = $utility->statsApiCall($graphXKey, $graphYKey, $endpoint, $method);
      $data = $file_contents[$graphYKey];
      $labels = $file_contents[$graphXKey];
    }

    $idPrefix = "graph_block_";

    $chart = $utility->renderChart($prefix, $configBlock, $gaphDataKey, $graphDataYKey, $data, $labels, $idPrefix);

    return $chart;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'graph_title' => '',
      'graph_type' => '',
      'graph_source' => '',
      'graph_description' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    $utility = new Utility();

    $graphResourceOptions = $utility->getGraphResources(TRUE);

    $graphTypesOptions = $utility->getGraphTypes();

    $form['graph_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Chart settings'),
    ];
    $form['graph_settings']['graph_title'] = [
      // This is the bootstrap class name that will be applied to the
      // wrapping column.
      // All form elements require a title.
      '#type' => 'textfield',
      '#title' => $this->t('Title of graph.'),
      // The field type.
      '#default_value' => $this->configuration['graph_title'],
      // These fields are specific to this form field type.
      '#placeholder' => $this->t('Chart title.'),
      '#required' => FALSE,
      '#attributes' => [
        'class' => ['ssa-grid-col-12'],
      ],
    ];
    $form['graph_settings']['graph_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Chart type.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['graph_type'],
      '#options' => $graphTypesOptions,
      '#attributes' => [
        'class' => ['ssa-grid-col-12'],
      ],
    ];
    $form['graph_settings']['graph_source'] = [
      '#type' => 'select',
      '#title' => $this->t('Data source of graph.'),
      '#required' => TRUE,
      '#default_value' => $this->configuration['graph_source'] ? $this->configuration['graph_source'] : 'example',
      // These fields are specific to this form field type.
      '#options' => $graphResourceOptions,
      '#attributes' => [
        'class' => ['ssa-grid-col-12'],
      ],
    ];
    $form['graph_settings']['graph_description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Chart description.'),
      '#required' => FALSE,
      '#default_value' => $this->configuration['graph_description']['value'] ?? '',
      '#attributes' => [
        'class' => ['ssa-grid-col-12'],
        'placeholder' => $this->t('Chart description text.'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (isset($values["graph_settings"]['graph_title'])) {
      $this->configuration['graph_title'] = $values["graph_settings"]['graph_title'];
    }
    if (isset($values["graph_settings"]['graph_description'])) {
      $this->configuration['graph_description'] = $values["graph_settings"]['graph_description'];
    }
    if (isset($values["graph_settings"]['graph_type'])) {
      $this->configuration['graph_type'] = $values["graph_settings"]['graph_type'];
    }
    if (isset($values["graph_settings"]['graph_source'])) {
      $this->configuration['graph_source'] = $values["graph_settings"]['graph_source'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    if ($form_state->getValue('graph_type') === 'John') {
      $form_state->setErrorByName('graph_type', $this->t('You can not say hello to John.'));
    }
  }

}
