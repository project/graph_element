<?php

namespace Drupal\graph_element\Plugin\CustomElement;

use Drupal\cohesion_elements\CustomElementPluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graph_element\Utility;

/**
 * Generic HTML element plugin for Site Studio (cohesion).
 *
 * @package Drupal\graph_element\Plugin\CustomElement
 *
 * @CustomElement(
 *   id = "graph_element",
 *   label = @Translation("Chart element")
 * )
 */
class Graph extends CustomElementPluginBase {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getFields() {
    $utility = new Utility();
    $graphResourceOptions = $utility->getGraphResources(TRUE);

    $graphTypesOptions = $utility->getGraphTypes();
    return [
      'graph_title' => [
        // This is the bootstrap class name that will be applied to the
        // wrapping column.
        'htmlClass' => 'ssa-grid-col-12',
        // All form elements require a title.
        'title' => $this->t('Title of graph.'),
        // The field type.
        'type' => 'textfield',
        // These fields are specific to this form field type.
        'placeholder' => $this->t('Placeholder text.'),
        'required' => FALSE,
        'validationMessage' => $this->t('This field is required.'),
      ],
      'graph_type' => [
        'htmlClass' => 'ssa-grid-col-12',
        'type' => 'select',
        'required' => TRUE,
        'title' => $this->t('Chart type.'),
        // These fields are specific to this form field type.
        'nullOption' => FALSE,
        'options' => $graphTypesOptions,
        'defaultValue' => 'donut',
      ],
      'graph_source' => [
        'htmlClass' => 'ssa-grid-col-12',
        'type' => 'select',
        'required' => TRUE,
        'title' => $this->t('Data source of graph.'),
        // These fields are specific to this form field type.
        'nullOption' => FALSE,
        'options' => $graphResourceOptions,
        'defaultValue' => 'example',
      ],
      'graph_description' => [
        'htmlClass' => 'ssa-grid-col-12',
        'type' => 'wysiwyg',
        'required' => FALSE,
        'title' => $this->t('Chart description.'),
        'defaultValue' => [
          'text' => '<p>This is some graph content.</p>',
          'textFormat' => 'cohesion',
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function render($element_settings, $element_markup, $element_class, $element_context = []) {
    if (!isset($element_settings['graph_source']) && empty($element_settings['graph_source'])) {
      return [];
    }
    $utility = new Utility();
    $graphResources = $utility->getGraphResources();

    // Prefix markup.
    $title = $element_settings['graph_title'] ?? '';
    $prefix = '<div class="element_graph_prefix_' . $element_settings['graph_type'] . '">';
    $prefix .= '<h3 class="graph_title">' . $title . '</h3>';
    $prefix .= isset($element_settings['graph_description']) ? $element_settings['graph_description']['text'] : '';
    $prefix .= '</div>';

    // Get the CSV contents.
    if ($element_settings['graph_source'] == "example") {
      $file_contents = $utility->getCsvContents();
      $gaphDataKey = '8.x-3.x';
      $graphDataYKey = 'Week';
      $data = array_reverse($file_contents['8.x-3.x']);
      $labels = array_reverse($file_contents['Week']);
    }
    else {
      $gaphDataKey = $graphResources[$element_settings['graph_source']]->x_key_name;
      $graphXKey = $graphResources[$element_settings['graph_source']]->x_key;
      $graphDataYKey = $graphResources[$element_settings['graph_source']]->y_key_name;
      $graphYKey = $graphResources[$element_settings['graph_source']]->y_key;
      $endpoint = $graphResources[$element_settings['graph_source']]->endpoint;
      $method = $graphResources[$element_settings['graph_source']]->method;
      $file_contents = $utility->statsApiCall($graphXKey, $graphYKey, $endpoint, $method);
      $data = $file_contents[$graphYKey];
      $labels = $file_contents[$graphXKey];
    }
    $idPrefix = "graph_element_";
    // Render the element.
    $chart = $utility->renderChart($prefix, $element_settings, $gaphDataKey, $graphDataYKey, $data, $labels, $idPrefix);

    return [
      '#theme' => 'graph_element',
      '#template' => 'graph-element-template',
      '#elementSettings' => $element_settings,
      '#elementMarkup' => $element_markup,
      '#elementContext' => $element_context,
      '#elementClass' => $element_class,
      '#elementChildren' => $chart,
    ];
  }

}
