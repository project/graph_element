<?php

namespace Drupal\graph_element\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Class for graph element settings form.
 *
 * @package Drupal\graph_element\Form
 */
class GraphRespurcesSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'graph_element_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['title'] = [
      '#type' => 'item',
      '#markup' => Markup::create('<h1>' . $this->t('Charts External Resources Settings') . '</h1>'),
    ];
    $form['description'] = [
      '#type' => 'item',
      '#markup' => Markup::create('<p>' . $this->t('This is settings page to configure graph external resources. You can configure here all resources that will be used in graph element, block and field options.') . '</p>'),
    ];

    $config = $this->config('graph_element.settings');
    $savedResources = $config->get('stats_types');

    $savedResourcdes = $form_state->get('stats_types');

    // We have to ensure that there is at least one name field.
    if ($savedResourcdes === NULL || $savedResourcdes == 0) {
      if ($savedResources === NULL || $savedResources == 0) {
        $savedResources = 1;
      }
      else {
        $savedResourcdes = count($savedResources);
      }
      $form_state->set('stats_types', $savedResourcdes);
      $savedResourcdes = $form_state->get('stats_types');
    }

    // Get a list of fields that were removed.
    $savedResourcdes = $form_state->get('stats_types');
    $removed_fields = $form_state->get('removed_fields');
    // If no fields have been removed yet we use an empty array.
    if ($removed_fields === NULL) {
      $form_state->set('removed_fields', []);
      $removed_fields = $form_state->get('removed_fields');
    }
    $form['#tree'] = TRUE;
    $form['fieldset_stats'] = [
      '#type' => 'details',
      '#title' => $this->t('Resource configuration'),
      '#open' => TRUE,
      '#description' => $this->t('All resources defined in your site and available for graph element.'),
      '#attributes' => [
        'class' => [
          'container-stats',
        ],
        'id' => [
          'fields-wrapper',
        ],

      ],
    ];
    for ($i = 0; $i < $savedResourcdes; $i++) {
      // Check if field was removed.
      if (in_array($i, $removed_fields)) {
        // Skip if field was removed and move to the next field.
        continue;
      }

      $title = isset($savedResources[$i]) && isset($savedResources[$i]['name']) ? ' - ' . $savedResources[$i]['name'] : (isset($savedResourcdes[$i]) && $savedResourcdes[$i]['name'] && !empty($savedResourcdes[$i]['name']) ? ' - ' . $savedResourcdes[$i]['name'] : '');
      $form['fieldset_stats'][$i] = [
        '#type' => 'details',
        '#title' => $this->t('Resource') . ' ' . ($i + 1) . $title,
        '#open' => !isset($savedResources[$i]) ? TRUE : FALSE,
      ];

      $form['fieldset_stats'][$i]['name'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Label'),
        '#size'          => 60,
        '#maxlength'     => 128,
        '#description'   => $this->t('Add a valid label'),
        '#required'      => TRUE,
        '#default_value' => isset($savedResources[$i]) && $savedResources[$i] && isset($savedResources[$i]['name']) ? $savedResources[$i]['name'] : '',

      ];
      $form['fieldset_stats'][$i]['endpoint'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Endpoint'),
        '#size'          => 60,
        '#maxlength'     => 128,
        '#description'   => $this->t('Add a valid endpoint'),
        '#required'      => TRUE,
        '#default_value' => isset($savedResources[$i]) && $savedResources[$i] && isset($savedResources[$i]['endpoint']) ? $savedResources[$i]['endpoint'] : '',

      ];
      $form['fieldset_stats'][$i]['method'] = [
        '#title' => $this->t('Method'),
        '#type' => 'select',
        '#description'   => $this->t('Select service method'),
        '#options' => [
          'GET' => $this->t('GET'),
          'POST' => $this->t('POST'),
        ],
        '#default_value' => isset($savedResources[$i]) && $savedResources[$i] && isset($savedResources[$i]['method']) ? $savedResources[$i]['method'] : '',
        '#required'      => TRUE,
        '#states' => [
          'visible' => [
            ':input[name="server_side_poc"]' => ['checked' => TRUE],
          ],
        ],
      ];

      $form['fieldset_stats'][$i]['x_key_name'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Data X Key label'),
        '#size'          => 60,
        '#maxlength'     => 128,
        '#description'   => $this->t('Add a valid label'),
        '#required'      => TRUE,
        '#default_value' => isset($savedResources[$i]) && $savedResources[$i] && isset($savedResources[$i]['x_key_name']) ? $savedResources[$i]['x_key_name'] : '',
      ];
      $form['fieldset_stats'][$i]['x_key'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Data X Key'),
        '#size'          => 60,
        '#maxlength'     => 128,
        '#description'   => $this->t('Add a valid key'),
        '#required'      => TRUE,
        '#default_value' => isset($savedResources[$i]) && $savedResources[$i] && isset($savedResources[$i]['x_key']) ? $savedResources[$i]['x_key'] : '',
      ];
      $form['fieldset_stats'][$i]['y_key_name'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Data Y Key label'),
        '#size'          => 60,
        '#maxlength'     => 128,
        '#description'   => $this->t('Add a valid label'),
        '#required'      => TRUE,
        '#default_value' => isset($savedResources[$i]) && $savedResources[$i] && isset($savedResources[$i]['y_key_name']) ? $savedResources[$i]['y_key_name'] : '',
      ];
      $form['fieldset_stats'][$i]['y_key'] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('Data Y Key'),
        '#size'          => 60,
        '#maxlength'     => 128,
        '#description'   => $this->t('Add a valid key'),
        '#required'      => TRUE,
        '#default_value' => isset($savedResources[$i]) && $savedResources[$i] && isset($savedResources[$i]['y_key']) ? $savedResources[$i]['y_key'] : '',
      ];
      $form['fieldset_stats'][$i]['actions'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#name' => $i,
        '#submit' => ['::removeCallback'],
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'fields-wrapper',
        ],
      ];
    }

    $form['fieldset_stats']['actions'] = [
      '#type' => 'actions',
    ];

    $form['fieldset_stats']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add one more resource'),
      '#submit' => ['::addOne'],
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'fields-wrapper',
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * Callback for both ajax-enabled buttons.
   *
   * Selects and returns the fieldset with the names in it.
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['fieldset_stats'];
  }

  /**
   * Submit handler for the "add-one-more" button.
   *
   * Increments the max counter and causes a rebuild.
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $savedResourcdes = $form_state->get('stats_types');
    $add_button = $savedResourcdes + 1;
    $form_state->set('stats_types', $add_button);
    $form_state->setRebuild();
  }

  /**
   * Submit handler for the "remove" button.
   *
   * Removes the corresponding line.
   */
  public function removeCallback(array &$form, FormStateInterface $form_state) {
    /*
     * We use the name of the remove button to find
     * the element we want to remove
     * Line 72: '#name' => $i,.
     */
    $trigger = $form_state->getTriggeringElement();
    $indexToRemove = $trigger['#name'];

    // Remove the fieldset from $form (the easy way)
    unset($form['fieldset_stats'][$indexToRemove]);

    // Remove the fieldset from $form_state (the hard way)
    // First fetch the fieldset, then edit it, then set it again
    // Form API does not allow us to directly edit the field.
    $namesFieldset = $form_state->getValue('fieldset_stats');
    unset($namesFieldset[$indexToRemove]);
    // $form_state->unsetValue('names_fieldset');
    $form_state->setValue('fieldset_stats', $namesFieldset);

    // Keep track of removed fields so we can add new fields at the bottom
    // Without this they would be added where a value was removed.
    $removed_fields = $form_state->get('removed_fields');
    $removed_fields[] = $indexToRemove;
    $form_state->set('removed_fields', $removed_fields);

    // Rebuild form_state.
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo Implement validations with validateForm() method.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $savedResourcdes = $form_state->get('stats_types');
    $result = [];

    for ($i = 0; $i < $savedResourcdes; $i++) {
      if (
        !empty($form_state->getValue(['fieldset_stats', $i, 'name']))
        &&
        !empty($form_state->getValue(['fieldset_stats', $i, 'endpoint']))
        &&
        !empty($form_state->getValue(['fieldset_stats', $i, 'x_key_name']))
        &&
        !empty($form_state->getValue(['fieldset_stats', $i, 'x_key']))
        &&
        !empty($form_state->getValue(['fieldset_stats', $i, 'y_key_name']))
        &&
        !empty($form_state->getValue(['fieldset_stats', $i, 'y_key']))
        ) {
        $result[$i]['name'] = $form_state->getValue(['fieldset_stats', $i, 'name']);
        $result[$i]['endpoint'] = $form_state->getValue(['fieldset_stats', $i, 'endpoint']);
        $result[$i]['method'] = $form_state->getValue(['fieldset_stats', $i, 'method']);
        $result[$i]['x_key_name'] = $form_state->getValue(['fieldset_stats', $i, 'x_key_name']);
        $result[$i]['x_key'] = $form_state->getValue(['fieldset_stats', $i, 'x_key']);
        $result[$i]['y_key_name'] = $form_state->getValue(['fieldset_stats', $i, 'y_key_name']);
        $result[$i]['y_key'] = $form_state->getValue(['fieldset_stats', $i, 'y_key']);
      }
    }

    $this->config('graph_element.settings')
      ->set('stats_types', $result)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get Editable config names.
   *
   * @inheritDoc
   */
  protected function getEditableConfigNames() {
    return ['graph_element.settings'];
  }

}
