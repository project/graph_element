# Charts external resources

This is a custom Drupal 10 module that provides a Graph Element.
It allows you to consume data from an external source and display
it as a chart.
The module provides a field, a block, and a Site Studio
custom element for displaying the chart.

## Features

- **External Source for Charts Module**: The module can consume data from an
  external source and use it to generate charts.
- **Field to Consume Data and Display Chart**: The module provides a field that
  can be added to content types. This field consumes data and displays a chart.
- **Block to Consume Data and Display Chart**: The module provides a block that
  can be placed in regions of your site.
  This block consumes data and displays a chart.
- **Site Studio Custom Chart Element**: The module provides a Site Studio
  custom element that can be used in Site Studio layouts. This custom element
  consumes data and displays it as a chart.

## Installation

1. Download the module using Composer: `composer require drupal/graph_element`
2. Enable the module in the Extend section of your Drupal admin dashboard.
3. Configure and enable: `/admin/config/content/charts/graph-resources`

## Configuration

1. Navigate to the Configuration section in your Drupal admin dashboard.
2. Click on Configuration -> Content authoring -> Graph resources settings
(`/admin/config/content/charts/graph-resources`).
3. Here you can set the Charts External resource settings.
4. Now you can add field, block or site studio element that use the
   graphs external resources and render the chart

## Permissions

The module provides the following permission:

- `administer charts resources settings`: Administer the module settings

## Maintainers

- Alberto Cocchiara (bigbabert)

## License

This project is licensed under the GPL-2.0-or-later.
