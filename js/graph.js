(function chart() {
  Drupal.graph_element = Drupal.graph_element || {};
  // PLEASE NOTE: this example is structured to work for Highcharts, but you
  // do the same thing for any of the charting libraries.
  Drupal.graph_element.highchartsTooltipFormatter = function chartToolTip() {
    let string = '';
    if (this.x) {
      string = `${Drupal.t('The value for')} <b>${this.x}</b> ${Drupal.t(
        'is',
      )} <b>${this.y}</b>`;
    } else {
      string = `${Drupal.t('The value for')} <b>${
        this.series.name
      }</b> ${Drupal.t('is')} <b>${this.y}</b>`;
    }
    return string;
  };
})();
